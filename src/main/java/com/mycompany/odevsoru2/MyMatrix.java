/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.odevsoru2;

import static java.lang.Math.sqrt;
import java.util.Random;

/**
 *
 * @author abdurrahman_sefer
 */
public class MyMatrix {

    final Random random = new Random();
    private int[][] m1;
    double ortalama = 0;

    double std = 0;

    public MyMatrix() {

    }

    public MyMatrix randMatrix(int m, int n) {
        
        if(m>=1&&n>=1)
        {
          this.m1 = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                this.m1[i][j] = random.nextInt(10);
            }
        }
        
        }else {
            System.out.println("Boyutlar hatalı girildi lutfen tekrar deneyiniz!!");

        }
        
        
      
        return this;
    }

    public static MyMatrix getInstance() {
        MyMatrix myMatrix = new MyMatrix();
        return myMatrix;
    }

    public MyMatrix println() {
        System.out.println();
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[i].length; j++) {
                System.out.print(m1[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println();
        return this;
    }

    public MyMatrix topla(MyMatrix a) {
        int row = a.m1.length;
        int column = a.m1[0].length;

        for (int r = 0; r < row; r++) {
            for (int c = 0; c < column; c++) {
                m1[r][c] = a.m1[r][c] + m1[r][c];
            }
        }
        return this;
    }

    public MyMatrix carp(MyMatrix a) {
        int row = a.m1.length;
        int column = a.m1[0].length;

        for (int r = 0; r < row; r++) {
            for (int c = 0; c < column; c++) {
                m1[r][c] = a.m1[r][c] * m1[r][c];
            }
        }
        return this;
    }

    public MyMatrix transpose() {
        int[][] temp = new int[m1[0].length][m1.length];
        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[0].length; j++) {
                temp[j][i] = m1[i][j];
            }
        }
        m1 = temp;

        return this;
    }



    public MyMatrix ortalama() {
        double sum = 0;

        int row = m1.length;
        int column = m1[0].length;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                sum = sum + m1[i][j];
            }
        }

        ortalama = sum / (row * column);
        System.out.println("ortalama = " + ortalama);
        return this;
    }

    double variance() {
        double sum = 0.0;
        int row = m1.length;
        int column = m1[0].length;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                double temp = 0;
                temp = m1[i][j] - ortalama;
                sum += temp * temp;
            }
        }

        return sum / ((row * column) - 1);
    }

    public MyMatrix std() {
        this.ortalama();

        double var = variance();

        std = sqrt(var);
        System.out.println("std = " + std);
        return this;
    }

    public MyMatrix sutunBazindaTopla() {
        int row = m1.length;
        int column = m1[0].length;
        for (int m = 0; m < column; m++) {
            int sutun_toplam = 0;
            for (int n = 0; n < row; n++) {
                sutun_toplam += m1[n][m];
            }
            System.out.println((m + 1) + ". Sutunun Toplami: " + sutun_toplam);
        }
        System.out.println();
        return this;
    }

    public MyMatrix subMatrix(int satir_1, int sutun_1, int satir_2, int sutun_2) {

        int row = (satir_2 - satir_1) + 1;
        int column = (sutun_2 - sutun_1) + 1;
        if (row >= 1 && column >= 1) {
            MyMatrix newMatrix = MyMatrix.getInstance().randMatrix(row, column);
            int i = 0;
            int j = 0;
            for (int r = satir_1 - 1; r < satir_2; r++) {
                for (int c = sutun_1 - 1; c < sutun_2; c++) {
                    newMatrix.m1[i][j] = m1[r][c];
                    j++;
                }
                j = 0;
                i++;
            }

            System.out.println();
            return newMatrix;
        } else {
            System.out.println("Boyutlar hatalı girildi lutfen tekrar deneyiniz!!");

        }

        return this;
    }
}
