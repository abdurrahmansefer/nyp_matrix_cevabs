/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.odevsoru2;

import java.net.MalformedURLException;

/**
 *
 * @author abdurrahman_sefer
 */
public class main_class {

    public static void main(String[] args) throws MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        MyMatrix m1 = MyMatrix.getInstance().randMatrix(10, 10).println();//10 satıra 10
        MyMatrix m2 = MyMatrix.getInstance().randMatrix(10, 10).println();//10 satıra 10
        MyMatrix m3 = m1.topla(m2).println();
        MyMatrix m4 = m1.carp(m2).println();
        MyMatrix m5 = m1.transpose().println();
        MyMatrix m6 = m1.ortalama().println();
        MyMatrix m7 = m1.std().println();
        MyMatrix m8 = m1.sutunBazindaTopla().println();
        MyMatrix m9 = m1.subMatrix(2, 3, 7, 5).println();

    }
}
